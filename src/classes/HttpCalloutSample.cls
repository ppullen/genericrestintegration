/**
* @author ppull
* @date 10/24/2017
* @description This class is a sample class for how to implement the HTTPCallout.class
**/


global with sharing class HttpCalloutSample {

    /**
    * @description builds the JSON, performs request and response actions
    */
    public static HttpResponse invokeCallout(){

        //Get standard configuration items
        Map<String, String> configMap = new Map<String, String>();
        Set<String> configSet = new Set<String>();
        configSet.add('END_POINT_URI');
        configSet.add('METHOD');
        configSet.add('CLIENT_CERTIFICATE');
        configMap = SOQLUtil.fetchSpecifiedKeyValuesInMetaData(configSet);

        //Get header configuration items
        Map<String, String> configHeaderMap = new Map<String, String>();
        configHeaderMap = SOQLUtil.fetchActiveHeaderKeyValuesInMetaData();

        //Construct JSON
        String jsonRequestString = HttpCalloutSample.constructRequestJSON();

        //Build HTTPCalloutVO
        //This generic structure is reusable by other HTTP callout integrations
        HTTPCalloutVO calloutValues = new HTTPCalloutVO();
        calloutValues.jsonRequestData = jsonRequestString;
        calloutValues.apiEndPointUri = configMap.get('END_POINT_URI');
        calloutValues.method = configMap.get('METHOD');
        calloutValues.clientCertificate = configMap.get('CLIENT_CERTIFICATE');
        calloutValues.headerInfo = configHeaderMap;

        //Invoke endpoint
        HTTPCalloutImpl callout = new HTTPCalloutImpl();
        HttpResponse responseObj = callout.invokeEndPoint(calloutValues);

        HttpCalloutResponseWrapperSample results = new HttpCalloutResponseWrapperSample();
        results = HttpCalloutSample.processResponse(responseObj);

        return responseObj;

        //Process response
        /*
        HttpCalloutResponseWrapperSample results = new HttpCalloutResponseWrapperSample();
        results = HttpCalloutSample.processResponse(responseObj);
        return results;
        */
    }

    /**
    * @description JSON is deserialized and processed into a Case sObject
    * @param response input reponse from the HTTPRequest
    */
    private static HttpCalloutResponseWrapperSample processResponse(HttpResponse response){

        //given a response, build the wrapper
        HttpCalloutResponseWrapperSample results = new HttpCalloutResponseWrapperSample();
        //results.status = response.getStatus();
        //results.statusCode = String.valueOf(response.getStatusCode());

        //get response data
        String jsonResponseData = response.getBody();
        system.debug('jsonResponseData: ' + jsonResponseData);

        //convert JSON object to Salesforce object
        system.Debug('Response data: ' + jsonResponseData);
        if (Integer.valueOf(response.getStatusCode()) == 200){
            results = (HttpCalloutResponseWrapperSample) JSON.deserialize(jsonResponseData, HttpCalloutResponseWrapperSample.class);
            system.debug('Results: ' + results);

            return results;
        }
        else{
            Logged_Exception__c exLog = new Logged_Exception__c( Who__c = UserInfo.getUserId(),
                    Session_Id__c = UserInfo.getSessionId(),
                    Organization_Id__c = UserInfo.getOrganizationId(),
                    Organization_Name__c = UserInfo.getOrganizationName() );

            String errorMessage = '';

            /*
            if(response.getStatusCode() != null){
                errorMessage += 'Status Code: ' + response.getStatusCode() + ' | ';
            }
            */

            if(results.statusCode != null){
                errorMessage += 'Status Code: ' + results.statusCode + ' | ';
            }

            if(results.statusMessage != null){
                errorMessage += 'Status Message: ' + results.statusMessage + ' | ';
            }

            exLog.Exception_Description__c = errorMessage;

            insert exLog;

            return null;
        }
    }

    /*
    private static MemberInfoResultsWrapper processResponse(String caseId, HttpResponse response){

        //given a response, build the wrapper
        MemberInfoResultsWrapper results = new MemberInfoResultsWrapper();
        results.responseStatus = response.getStatus();
        results.responseStatusCode = String.valueOf(response.getStatusCode());

        //get response data
        String jsonResponseData = response.getBody();

        //convert JSON object to Salesforce object
        List<Case> caseObjList = new List<Case>();
        if(string.isNotBlank(caseId)){
            if (Integer.valueOf(response.getStatusCode()) == 200){

                jsonResponseData = jsonResponseData.replace('"group" :', '"group_x" :');

                MemberInfo_ResponseWrapper caseResponse = new MemberInfo_ResponseWrapper();

                caseResponse = (MemberInfo_ResponseWrapper)JSON.deserialize(jsonResponseData, MemberInfo_ResponseWrapper.class);

                if(caseResponse.eligibilityBenefits == null){
                    caseResponse.eligibilityBenefits = new MemberInfo_ResponseWrapper.EligibilityBenefits();
                }


                for (MemberInfo_ResponseWrapper.Member cb : caseResponse.eligibilityBenefits.Member) {

                    Case caseObj = new Case(Id = caseId);

                    //Member Class
                    caseObj.External_ID__c = cb.subscriberId;
                    caseObj.First_Name__c = cb.givenName;
                    caseObj.Middle_Initial__c = cb.middleName;
                    caseObj.Last_Name__c = cb.familyName;

                    caseObj.Birth_Date__c = cb.birthDate != null ? date.valueOf(cb.birthDate) : null;

                    caseObj.Gender__c = cb.subscriberId;
                    caseObj.SSN__c = cb.socialSecurityNumber != null ? decimal.valueOf(cb.socialSecurityNumber) : 0;
                    caseObj.Subscriber_Relationship_Description__c = cb.subscriberRelationshipDescription;
                    caseObj.Dependant_Number__c = cb.dependentNumber;
                    caseObj.Age__c = cb.ageNumber;
                    caseObj.Alphaprefix__c = cb.alphaPrefixCode;
                    caseObj.Member_ID_Display__c = cb.displayedMemberId != null ? cb.displayedMemberId : '';

                    if(cb.group_x != null && !cb.group_x.isEmpty()) {
                        for (MemberInfo_ResponseWrapper.Group_x grp : cb.group_x) {
                            //Group
                            caseObj.Parent_Group__c = grp.parentGroupId;
                            caseObj.Group_Name__c = grp.groupName;
                            caseObj.Group_Number__c = grp.groupNumber;
                            caseObj.Funding_Arrangement_Code__c = grp.fundingArrangementCode;
                            caseObj.Market_Segment_ACC__c = grp.marketSegment;

                            for (MemberInfo_ResponseWrapper.GroupContract grpCon : grp.groupContract) {
                                for (MemberInfo_ResponseWrapper.GroupContractProductOffering grpConPO : grpCon.groupContractProductOffering) {
                                    //GroupContractProductOffering
                                    caseObj.Group_Benefit_Package_Effective_Date__c = date.valueOf(grpConPO.effectiveDate);
                                    caseObj.Group_Benefit_Package_Expiration_Date__c = date.valueOf(grpConPO.terminationDate);
                                }
                            }
                        }
                    }

                    if(!cb.policy.isEmpty()) {
                        for (MemberInfo_ResponseWrapper.Policy pol : cb.policy) {
                            //Policy
                            caseObj.Source_System__c = pol.enrollmentSourceSystemCode;
                            caseObj.Billing_Exclusion_Indicator__c = pol.excludeFromBillingIndicator;

                            for (MemberInfo_ResponseWrapper.LineOfBusiness lob : pol.lineOfBusiness) {
                                //LineOfBusiness
                                caseObj.LOB_Desc__c = lob.lineOfBusinessDescription;
                            }
                        }
                    }

                    if(!cb.policyMemberShip.isEmpty()) {
                        for (MemberInfo_ResponseWrapper.PolicyMemberShip polMem : cb.policyMemberShip) {
                            //PolicyMemberShip
                            caseObj.Member_Effective_Date__c = date.valueOf(polMem.enrollmentEffectiveDate);
                            caseObj.Member_Expiration_Date__c = date.valueOf(polMem.enrollmentExpirationDate);
                            caseObj.On_Exchange__c = polMem.memberOnExchangeIndicator;
                            caseObj.Blue_Rewards_Flag__c = polMem.blueRewardsIndicator;
                        }
                    }

                    if(!cb.memberContact.isEmpty()) {
                        for (MemberInfo_ResponseWrapper.MemberContact memCon : cb.memberContact) {
                            for (MemberInfo_ResponseWrapper.EnrolledMemberAddress ema : memCon.enrolledMemberAddress) {
                                //EnrolledMemberAddress
                                caseObj.Address_Line_1__c = ema.addressLine1;
                                caseObj.Address_Line_2__c = ema.addressLine2;
                                caseObj.City__c = ema.cityName;
                                caseObj.State__c = ema.stateCode;
                                caseObj.Zip_Code__c = ema.postalCode;
                                caseObj.Conf_Comm_Active__c = ema.confidentialCommunicationRequestIndicator;
                            }
                        }
                    }

                    if(!cb.productOffering.isEmpty()) {
                        for (MemberInfo_ResponseWrapper.ProductOffering2 prodOff2 : cb.productOffering) {
                            //ProductOffering2
                            if(prodOff2.benefitOptionGroupTypeCode == 'MED') {
                                caseObj.Non_Standard_Indicator__c = prodOff2.nonStandardIndicator;
                                caseObj.Product_Metallic_Code__c = prodOff2.productMetallicCode;
                                caseObj.HDHP_Indicator__c = prodOff2.highDeductibleHealthPlanIndicator;
                                caseObj.ACA_Indicator__c = prodOff2.acaIndicator;
                                caseObj.Grandfather_Flag__c = prodOff2.grandfatheredIndicator;
                                caseObj.Smoker_Indicator__c = prodOff2.smokerIndicator;
                                caseObj.True_Out_of_Pocket_Indicator__c = prodOff2.trueOutOfPocketIndicator;
                            }
                        }
                    }
                    caseObjList.add(caseObj);
                }
            }
            else
            {
                //construct error message and object
                jsonResponseData = jsonResponseData.replace('"error Code"', '"errorCode"');
                jsonResponseData = jsonResponseData.replace('"error Message"', '"errorMessage"');
                jsonResponseData = jsonResponseData.replace('"Content-Type"', '"contentType"');
                jsonResponseData = jsonResponseData.replace('"transaction-id"', '"transactionId"');

                String errorMessage = '';

                if(results.responseStatusCode != null){
                    errorMessage += 'Response Code: ' + results.responseStatusCode + ' | ';
                }
                else if(results.errorCode != null){
                    errorMessage += 'Error Code Code: ' + results.errorCode + ' | ';
                }

                if(results.errorMessage != null){
                    errorMessage += 'Error Message: ' + results.errorMessage + ' | ';
                }

                if(results.transactionId != null){
                    errorMessage += 'Transaction Id: ' + results.transactionId + ' | ';
                }

                if(results.errorTrace != null){
                    errorMessage += 'Error Trace: ' + results.errorTrace + ' | ';
                }

                results = (MemberInfoResultsWrapper) JSON.deserialize(jsonResponseData, MemberInfoResultsWrapper.class);
                Apex_Error__c errorlog = new Apex_Error__c(
                    Method_Name__c = 'MemberInfo_Callout.processResponse',
                    Short_Description__c = errorMessage,
                    Error_Message__c = '',
                    Record_Data__c = caseId

                );
                insert errorlog;
                results.errorLog = errorlog;
            }

            results.caseList = caseObjList;
            return results;
        }
        else{
            return null;
        }
    }
    */

    /**
    * @description given values, construct request JSON string
    */
    public static String constructRequestJSON(){

        //can use request wrapper for serialization, or build request JSON manually, as I have done below
        //manual map construction for serialization

        Map <String, Object> childMemberValues = new Map <String, Object>();
        childMemberValues.put('child member', 'NA');

        Map <String, List<Object>> parentMemberValues = new Map <String, List<Object>>();
        parentMemberValues.put('parent member', new List<Object>{childMemberValues});

        Map <String, Object> grandParentMemberValues = new Map <String, Object>();
        grandParentMemberValues.put('grand parent member', parentMemberValues);

        //serialize wrapper into JSON
        String jsonString = JSON.serialize(grandParentMemberValues);

        return jsonString;
    }
    /*
    public static String constructMemberInfoJSON(Id caseId, String searchType, String searchValue){

        Map <String, Object> memberValues = new Map <String, Object>();
        if(searchType == 'memberId'){
            memberValues.put('memberId', searchValue);
        }
        else if(searchType == 'socialSecurityNumber'){
            memberValues.put('socialSecurityNumber', searchValue);
        }

        Map <String, List<Object>> member = new Map <String, List<Object>>();
        member.put('member', new List<Object>{memberValues});

        Map <String, Object> eligibilityBenefits = new Map <String, Object>();
        eligibilityBenefits.put('eligibilityBenefits', member);

        //serialize wrapper into JSON
        String jsonString = JSON.serialize(eligibilityBenefits);

        return jsonString;
    }
    */
}