/**
* @author ppull
* @date 10/24/2017
* @description This is a generic structure that can be passed to HTTPCalloutImpl.
* @description Will work for any type of API integration
**/


public class HTTPCalloutVO {

    //JSON request
    public String jsonRequestData {get;set;}

    //Used to reference endpoint URI from Apex Config custom metadata
    public String apiEndPointUri {get;set;}

    //HTTP header property, POST,GET,etc
    public String method {get;set;}

    //HTTP Client Certificate.  Stored in MDT
    public String clientCertificate {get;set;}

    //Header values to include in callout.  Stored in MDT
    public Map<String, String> headerInfo {get;set;}
}