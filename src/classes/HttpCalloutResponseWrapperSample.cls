/**
* @author ppull
* @date 10/24/2017
* @description Wrapper for Memberinfo HTTP Response
**/


public with sharing class HttpCalloutResponseWrapperSample {
    //public String status {get;set;}
    public String statusCode {get;set;}
    public String statusMessage {get; set;}
}