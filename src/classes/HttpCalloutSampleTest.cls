/**
* @author ppull
* @date 10/24/2017
* @description unit tests for the MemberInfo Integrations
**/


@isTest
public with sharing class HttpCalloutSampleTest {

    /**
    * Method Tested: HttpCalloutSample
    * | Scenario: performs HTTPCallout to retrieve MemberInfo (200)
    * | Expected Result: status code message 'HTTP/1.1 200 Success' should be returned
    **/
    static TestMethod void HttpCalloutSample_200() {
        Test.setMock(HttpCalloutMock.class, new HttpCalloutMock_200());

        HttpResponse response = new HttpResponse();
        Test.startTest();
            response = HttpCalloutSample.invokeCallout();
        Test.stopTest();

        System.assertEquals(200, response.getStatusCode());
    }

    /**
    * Method Tested: HttpCalloutSample
    * | Scenario: performs HTTPCallout to retrieve MemberInfo (400)
    * | Expected Result: status code message 'HTTP/1.1 200 Success' should be returned
    **/
    static TestMethod void HttpCalloutSample_400() {
        Test.setMock(HttpCalloutMock.class, new HttpCalloutMock_400());

        HttpResponse response = new HttpResponse();

        Test.startTest();
            response = HttpCalloutSample.invokeCallout();
        Test.stopTest();

        System.assertEquals(400, response.getStatusCode());
    }

    /**
    * Method Tested: HttpCalloutSample
    * | Scenario: performs HTTPCallout to retrieve MemberInfo (404)
    * | Expected Result: status code message 'HTTP/1.1 404 Success' should be returned
    **/
    static TestMethod void HttpCalloutSample_404() {
        Test.setMock(HttpCalloutMock.class, new HttpCalloutMock_404());

        HttpResponse response = new HttpResponse();

        Test.startTest();
            response = HttpCalloutSample.invokeCallout();
        Test.stopTest();

        System.assertEquals(404, response.getStatusCode());
    }

    /**
    * Method Tested: HttpCalloutSample
    * | Scenario: performs HTTPCallout to retrieve MemberInfo (500)
    * | Expected Result: status code message 'HTTP/1.1 500 Success' should be returned
    **/
    static TestMethod void HttpCalloutSample_500() {
        Test.setMock(HttpCalloutMock.class, new HttpCalloutMock_500());

        HttpResponse response = new HttpResponse();

        Test.startTest();
            response = HttpCalloutSample.invokeCallout();
        Test.stopTest();

        System.assertEquals(500, response.getStatusCode());
    }
}