/**
* @author ppull
* @date 10/24/2017
* @description Mock for MemberInfo_Continuation_ControllerTest
**/

global with sharing class HttpCalloutMock_400 implements HttpCalloutMock{
    /**
    * @description: creates mock HTTPRESPONSE
    **/
    global HTTPResponse respond(HTTPRequest req) {

        //String mockJson = '{"status": "HTTP/1.1 400 Bad Request"}';
        String mockJson = '{"statusCode": "400", "statusMessage": "HTTP/1.1 400 Bad Request"}';

        // Create a mock response
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody(mockJson);
        res.setStatusCode(400);
        return res;
    }
}