/**
* @author Pike Pullen
* @date 10/24/2017
* @description Interface for HTTPCalloutImpl
**/


public interface HTTPCallout {

    HttpResponse invokeEndPoint(HTTPCalloutVO callout);
}