/**
* @author ppull
* @date 10/24/2017
* @description SOQL for integrations
**/

public class SOQLUtil {


    /**
    * @description Queries Custom Metadata Type 'Costing_Callout_Setting__mdt', returns specified values
    **/
    public static Map<String, String> fetchSpecifiedKeyValuesInMetaData_CostingCallout(Set<String> recordsToBeFetched) {
        Map<String, String> keyValueMap = new Map<String, String>();

        for (Costing_Callout_Setting__mdt httpConfig : [SELECT DeveloperName, Header_Key__c, Value__c
        FROM Costing_Callout_Setting__mdt
        WHERE IsActive__c = true
        AND DeveloperName IN: recordsToBeFetched]) {

            keyValueMap.put(httpConfig.DeveloperName, httpConfig.Value__c) ;
        }

        return keyValueMap;
    }

    /**
    * @description Queries Custom Metadata Type 'Costing_Callout_Setting__mdt', returns all header values
    **/
    public static Map<String, String> fetchActiveHeaderKeyValuesInMetaData_CostingCallout() {
        Map<String, String> keyValueMap = new Map<String, String>();

        for (Costing_Callout_Setting__mdt httpConfig : [SELECT DeveloperName, Header_Key__c, Value__c
        FROM Costing_Callout_Setting__mdt
        WHERE IsActive__c = true
        AND IsAuthHeader__c = true]) {
            keyValueMap.put(httpConfig.Header_Key__c, httpConfig.Value__c) ;
        }

        return keyValueMap;
    }

    /**
    * @description Queries Custom Metadata Type 'Http_Callout_Default_Setting__mdt', returns specified values
    **/
    public static Map<String, String> fetchSpecifiedKeyValuesInMetaData(Set<String> recordsToBeFetched) {
        Map<String, String> keyValueMap = new Map<String, String>();

        for (Http_Callout_Default_Setting__mdt httpConfig : [SELECT DeveloperName, Header_Key__c, Value__c
        FROM Http_Callout_Default_Setting__mdt
        WHERE IsActive__c = true
        AND DeveloperName IN: recordsToBeFetched]) {

            keyValueMap.put(httpConfig.DeveloperName, httpConfig.Value__c) ;
        }

        return keyValueMap;
    }

    /**
    * @description Queries Custom Metadata Type 'Http_Callout_Default_Setting__mdt', returns all header values
    **/
    public static Map<String, String> fetchActiveHeaderKeyValuesInMetaData() {
        Map<String, String> keyValueMap = new Map<String, String>();

        for (Http_Callout_Default_Setting__mdt httpConfig : [SELECT DeveloperName, Header_Key__c, Value__c
        FROM Http_Callout_Default_Setting__mdt
        WHERE IsActive__c = true
        AND IsAuthHeader__c = true]) {
            keyValueMap.put(httpConfig.Header_Key__c, httpConfig.Value__c) ;
        }

        return keyValueMap;
    }
}