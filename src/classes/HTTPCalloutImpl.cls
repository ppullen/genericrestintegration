/**
* @author ppull
* @date 10/24/2017
* @description Handles callouts via HTTPCallout Interface.
* @description  This is a heavily generic class that can be used for all future integrations.
**/


global class HTTPCalloutImpl implements HTTPCallout {

    /*
    * @description This performs HTTP Request, generic for all other callouts.
    * @param callout This is wrapper for all the request data
    */
    public HttpResponse invokeEndPoint(HTTPCalloutVO callout) {

        HttpRequest request = new HttpRequest();
        request.setEndpoint(callout.apiEndPointUri);
        request.setMethod(callout.method);
        request.setBody(callout.jsonRequestData);
        if(String.isNotBlank(callout.clientCertificate)) {
            request.setClientCertificateName(callout.clientCertificate);
        }

        for(String str : callout.headerInfo.keySet()) {
            request.setHeader(str, callout.headerInfo.get(str));
        }

        system.debug('HTTP REQUEST ENDPOINT: ' + request.getEndpoint());
        system.debug('HTTP REQUEST METHOD: ' + request.getMethod());
        system.debug('HTTP REQUEST BODY: ' + request.getBody());

        HttpResponse response = new HttpResponse();
        Http httpCallout = new Http();
        response = httpCallout.send(request);
        system.debug('HTTP RESPONSE: (' + response.getStatusCode() + ') - ' + response.getStatus());

        return response;
    }
}